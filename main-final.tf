# main.tf

provider "aws" {
  region = "us-east-1" 
}

# Crear la VPC
resource "aws_vpc" "istea_vpc" {                                 
    cidr_block = "10.0.0.0/16"                                   
    instance_tenancy = "default"                                 

    tags = {                                                     
      Name = "prueba_istea_1"
    }
}

# Crear la subred pública
resource "aws_subnet" "sn_publica_1" {                          
    vpc_id = aws_vpc.istea_vpc.id                                
    cidr_block = "10.0.1.0/24"                                   
    availability_zone = "us-east-1a"                             

    tags = {                                                     
      Name = "sn_publica_1"
    }
}

# Crear el Internet Gateway
resource "aws_internet_gateway" "ig_publica_1" {                
    vpc_id = aws_vpc.istea_vpc.id                                

    tags = {                                                     
      Name = "ig_publica_1"
    }
}

# Crear la ruta para la subred pública
resource "aws_route_table" "rt_publica_1" {                      
    vpc_id = aws_vpc.istea_vpc.id                                

    tags = {                                                     
      Name = "rt_publica_1"
    }
}

resource "aws_route" "acceso_internet" {                          
    route_table_id = aws_route_table.rt_publica_1.id              
    destination_cidr_block = "0.0.0.0/0"                          
    gateway_id = aws_internet_gateway.ig_publica_1.id             
}

# Asociar la ruta pública con la subred
resource "aws_route_table_association" "association_publica_1" {  
    subnet_id      = aws_subnet.sn_publica_1.id                   
    route_table_id = aws_route_table.rt_publica_1.id              
}

# Crear un grupo de seguridad
resource "aws_security_group" "allow_ssh" {
  vpc_id = aws_vpc.istea_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_ssh"
  }
}

# Crear una instancia EC2
resource "aws_instance" "web" {
  ami           = "ami-0fe630eb857a6ec83" 
  instance_type = "t2.micro"

  subnet_id              = aws_subnet.sn_publica_1.id
  associate_public_ip_address = true
  key_name = "Instancia_Publica"
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]

  tags = {
    Name = "Instancia_Publica"
  }
}
